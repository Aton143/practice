#include <stdio.h>

extern "C" unsigned int _test(unsigned int, unsigned int);

int main()
{
  unsigned int result = _test(1, 2);
  printf("%d\n", result);

  return(0);
}

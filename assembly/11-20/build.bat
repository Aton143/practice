@echo off

REM set CompilerFlags=-MTd -nologo -Gm- -GR- -EHa- -Od -Oi -fp:fast -WX -W4 -FC -Z7
REM set LinkerFlags=-incremental:no -opt:ref
REM cl %CompilerFlags% .\assembly_practice.obj .\%FileName%.obj /link %LinkerFlags%

set FileName=hello_world

IF NOT EXIST .\build mkdir .\build
pushd .\build

REM -a: Target architecture
REM -f: Object format
REM -m: Target machine architecture
REM -o: Object filename
REM -p: Parser
REM -r: Preprocessor
REM -X: Change error/warning reporting style
REM 

yasm -f win64 -p nasm -r nasm -Werror -Worphan-labels -X vc -o %FileName%.obj ..\%FileName%.asm 
link %FileName%.obj kernel32.lib ucrt.lib legacy_stdio_definitions.lib msvcrt.lib /subsystem:console /out:%FileName%.exe -nologo

popd

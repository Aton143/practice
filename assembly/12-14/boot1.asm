bits 16                 ; this is 16-bit code
org 0x7c00              ; output at offset 0x7c00
boot: 
  mov si, hello         ; move si register to hello label memory location
  mov ah, 0x0e          ; "Write Character in TTY mode"

.loop:
  lodsb                 ; load hello string
  or    al, al          ; is al == 0?
  jz    halt

  int   0x10            ; 0x10 -> Video Services
  jmp  .loop

halt:
  cli                   ; clear interrupt flag
  hlt                   ; halt execution
hello: db "Hello world!", 0x0

times 510 - ($-$$) db 0 ; pad remaining 510 bytes with zeroes
dw 0xaa55               ; bootloader magic - marks this 512-byte sector bootable!

bits 64
default rel

segment .data
  float_constant dd 1.0

segment .text
  global float_add

float_add:
  push  rbp
  mov   rbp, rsp
  sub   rsp, 32

  call  float_constant_add
  addss xmm0, xmm1

  leave
  ret

float_constant_add:
  push  rbp
  mov   rbp, rsp
  sub   rsp, 32

  movss xmm2, dword [float_constant]
  addss xmm0, xmm2

  leave
  ret

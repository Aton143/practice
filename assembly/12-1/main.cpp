#include <stdio.h>

extern "C" float float_add(float a, float b);

// might need __cdecl
extern "C" int main(void) {
  printf("%f + %f + %f = %f\n", 10.0f, 20.0f, 1.0f, float_add(10.0f, 20.0f));

  return(0);
}

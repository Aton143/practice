@echo off

IF NOT EXIST build mkdir build

del /Q build\*

pushd build

yasm ..\float.asm -o float.obj -f win64 -m amd64 -X vc -g cv8 -Werror -Worphan-labels
link float.obj /dll /nologo /defaultlib:shell32.lib /defaultlib:kernel32.lib /defaultlib:msvcrt.lib /defaultlib:ucrt.lib /defaultlib:legacy_stdio_definitions.lib /incremental:no /debug /opt:ref /export:float_add
cl ..\main.cpp /nologo /link /entry:main /defaultlib:ucrt.lib /subsystem:console /NODEFAULTLIB:library /defaultlib:shell32.lib /debug /defaultlib:kernel32.lib /defaultlib:float.lib /incremental:no

popd

bits 64
default rel

segment .text
global add2

add2:
  push rbp
  mov  rbp, rsp
  sub  rsp, 32

  add  rcx, rdx
  mov  rax, rcx

  leave
  ret

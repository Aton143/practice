#include <stdio.h>

int main(void) {
  int a = 45;
  int b = 99;

  int c = add2(a, b);
  printf("%d + %d = %d\n", a, b, c);

  return 0;
}

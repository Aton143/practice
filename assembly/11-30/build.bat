@echo off

IF NOT EXIST build mkdir build

del /Q build\*

pushd build

yasm -f win64 -Werror -Worphan-labels -X vc -g cv8 -o add.obj ..\add.asm
link add.obj /dll /defaultlib:msvcrt.lib /defaultlib:legacy_stdio_definitions.lib /defaultlib:ucrt.lib /defaultlib:kernel32.lib /defaultlib:shell32.lib /opt:ref /export:add2 /incremental:no /debug /nologo 
cl ..\main.c /Ox /nologo /link /defaultlib:msvcrt.lib /defaultlib:legacy_stdio_definitions.lib /defaultlib:ucrt.lib /defaultlib:kernel32.lib /defaultlib:shell32.lib /subsystem:console /debug /incremental:no /entry:main /opt:ref add.lib /machine:x64

popd

@echo off

IF NOT EXIST build mkdir build

del /Q build\*

pushd build

yasm -a x86 -f win64 -m amd64 -g cv8 -X vc -Werror -o rdtsc.obj ..\rdtsc.asm
link rdtsc.obj /dll /defaultlib:ucrt.lib /defaultlib:msvcrt.lib /nologo /incremental:no /opt:ref /export:read_timer /export:read_timer_after_all_previous_loads /export:read_timer_after_all_previous_loads_and_writes /export:read_timer_before_all_future_instructions_and_memory_accesses /DEBUG:FULL /out:rdtsc.dll
cl ..\main.cpp /Zi /link /subsystem:console /nologo /incremental:no /opt:noref /defaultlib:rdtsc.lib

popd

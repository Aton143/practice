#include <stdio.h>
#include <stdint.h>

extern "C" uint64_t read_timer(void);
extern "C" uint64_t read_timer_after_all_previous_loads(void);
extern "C" uint64_t read_timer_after_all_previous_loads_and_writes(void);
extern "C" uint64_t read_timer_before_all_future_instructions_and_memory_accesses(void);

int main(void) {
  printf("%llx\n", read_timer());
  return 0;
}

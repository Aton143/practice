bits 64
default rel

segment .text
global read_timer
global read_timer_after_all_previous_loads
global read_timer_after_all_previous_loads_and_writes
global read_timer_before_all_future_instructions_and_memory_accesses

; these are fudgy because of the function prologue/epilogue
read_timer:
  push rbp
  mov  rbp, rsp

  rdtsc

  shl  rdx, 0x20
  or   rax, rdx

  leave
  ret

read_timer_after_all_previous_loads:
  push rbp
  mov  rbp, rsp

  lfence

  rdtsc

  shl  rdx, 0x20
  or   rax, rdx

  leave
  ret

read_timer_after_all_previous_loads_and_writes:
  push rbp
  mov  rbp, rsp

  mfence
  lfence

  rdtsc

  shl  rdx, 0x20
  or   rax, rdx

  leave
  ret

read_timer_before_all_future_instructions_and_memory_accesses:
  push rbp
  mov  rbp, rsp

  rdtsc
  lfence

  shl  rdx, 0x20
  or   rax, rdx

  leave
  ret

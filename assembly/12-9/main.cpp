#include <stdio.h>
#include <stdint.h>

extern "C" int32_t reverse_int32(int32_t n);

int main(void) {
  int32_t n = -123;
  int32_t r = reverse_int32(n);

  printf("n: %d\nr: %d\n", n, r);
  return 0;
}

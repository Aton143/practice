bits 64
default rel

segment .text
global reverse_int32

reverse_int32:
  push rbp
  mov  rbp, rsp
  sub  rsp, 0x20

  xor  eax, eax ; return value
  mov  ebx, ecx

  mov  ecx, 0x20

.bit_loop:
  shl  eax, 0x1 ; eax <<= 1

  mov  edi, ebx
  and  edi, 0x1

  or   eax, edi ; eax = (ecx & 1)

  shr  ebx, 0x1

  dec  ecx

  cmp  ebx, 0x0
  jnz  .bit_loop 

  cmp  ecx, 0x0
  je   .leave_func

  shl  eax, cl

.leave_func:
  leave
  ret

@echo off

IF NOT EXIST build mkdir build

del /Q build\*

pushd build

yasm -a x86 -f win64 -m amd64 -g cv8 -X vc -Werror -o reverse.obj ..\reverse.asm
link reverse.obj /dll /nologo /debug:full /incremental:no /opt:ref /export:reverse_int32 /defaultlib:msvcrt.lib /defaultlib:ucrt.lib /out:reverse.dll
cl ..\main.cpp /Zi /link /subsystem:console /incremental:no /opt:noref /defaultlib:reverse.lib /nologo

popd

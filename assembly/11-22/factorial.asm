bits 64
default rel

segment .data
  fmt db "factorial is: %d", 0xD, 0xA, 0

segment .text
global main

extern _CRT_INIT
extern printf
extern ExitProcess

factorial:
  push rbp
  mov  rbp, rsp
  sub  rsp, 32

  test ecx, ecx ; n
  jz  .zero

  mov  ebx, 1    ; counter
  mov  eax, 1    ; result

  inc  ecx       ; n++

.for_loop:
  cmp  ebx, ecx  ; ebx == n
  je   .end_loop

  mul  ebx       ; eax = ebx * eax (= result *= counter)
  inc  ebx       ; ++c

  jmp  .for_loop

.zero:
  mov  eax, 1

.end_loop:
  leave
  ret


main:
  push rbp
  mov  rbp, rsp
  sub  rsp, 32

  call _CRT_INIT

  mov  rcx, 4     ; n
  call factorial

  lea  rcx, [fmt] ; rcx = &fmt[0]
  mov  rdx, rax   ; rdx = factorial(rcx) (= factorial(n))
  call printf     ; printf(rax, rdx) (= printf(&fmt[0], factorial(n)))

  xor  rax, rax
  call ExitProcess

@echo off

IF NOT EXIST build mkdir build

del /Q build\*

pushd build

yasm -f win64 -g cv8 -m amd64 -o factorial.obj -Werror -Worphan-labels -X vc ..\factorial.asm
link factorial.obj legacy_stdio_definitions.lib kernel32.lib msvcrt.lib shell32.lib /subsystem:console /out:factorial.exe -nologo -incremental:no -opt:noref -debug

popd 

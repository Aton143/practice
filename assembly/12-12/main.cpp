#include <stdio.h>
#include <stdint.h>

extern "C" float population_deviation(float *aligned_floats, int32_t aligned_floats_count);

int main(void) {
  __declspec(align(16)) float aligned_floats[] = {0.1f};
  int32_t aligned_floats_count = sizeof(aligned_floats) / sizeof(*aligned_floats);

  float standard_deviation = population_deviation(aligned_floats, aligned_floats_count);
  printf("standard deviation of population: %f\n", standard_deviation);

  return 0;
}

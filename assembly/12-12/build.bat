@echo off

IF NOT EXIST build mkdir build

del /Q build\*

pushd build

yasm -a x86 -f win64 -m amd64 -g cv8 -X vc -Werror -o deviation.obj ..\deviation.asm
link deviation.obj /dll /debug:full /opt:ref /export:population_deviation /out:deviation.dll /nologo /defaultlib:ucrt.lib /defaultlib:msvcrt.lib /incremental:no
cl ..\main.cpp /Zi /link /subsystem:console /defaultlib:deviation.lib /debug:full /nologo /incremental:no /opt:noref

popd

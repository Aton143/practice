bits 64
default rel

segment .data
global population_deviation

population_deviation:
  push     rbp
  mov      rbp, rsp

  pxor     xmm1, xmm1 ; sum of  x_i 
  pxor     xmm0, xmm0 ; sum of (x_i)^2

  mov      edi,   edx
  cmp      rdx,   0x3
  jle     .loop

.loop:
  movaps   xmm2, [rcx]
  addps    xmm1,  xmm2

  mulps    xmm2,  xmm2
  addps    xmm0,  xmm2

  add      rcx,   0x10
  sub      rdx,   0x4

  cmp      rdx,   0x3
  jg      .loop

.residuals:
  cmp      rdx,   0x0
  jl      .end

  movss    xmm2, [rcx]
  addss    xmm1,  xmm2

  mulss    xmm2,  xmm2
  addss    xmm0,  xmm2

  jmp     .residuals

.end:
  cvtsi2ss xmm2, edi

  haddps   xmm0, xmm0
  haddps   xmm0, xmm0

  haddps   xmm1, xmm1
  haddps   xmm1, xmm1

  mulss    xmm1, xmm1
  divss    xmm0, xmm2

  subss    xmm1, xmm0
  movss    xmm0, xmm1

  leave
  ret

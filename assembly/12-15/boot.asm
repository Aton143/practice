bits 16
org 0x7c00

boot:
  mov   ax, 0x2401
  int   0x15           ; enable A20 bit to access all memory
  mov   ax, 0x3
  int   0x10           ; enable VGA text mode 3 - for more information: https://en.wikipedia.org/wiki/VGA_text_mode
  cli
  lgdt [gdt_pointer]   ; load the Global Descriptor Table
  mov   eax, cr0 
  or    eax, 0x1       ; set protected mode on - for more information: https://en.wikipedia.org/wiki/Control_register
  mov   cr0, eax

  jmp   CODE_SEG:boot2 ; long jump to the code segment

; gdt_start               gdt_code      gdt_data
;  0  1  2  3  4  5  6  7  8  9  A  B  C  D  E  F 10 11 12 13
; 00 00 00 00 00 00 00 00 FF FF 9A CF 00 FF FF 00 00 92 CF 00
; ??

gdt_start:
  dq    0x0
gdt_code:
  dw    0xFFFF
  dw    0x0
  db    0x0
  db    10011010b
  db    11001111b
  db    0x0
gdt_data:
  dw    0xFFFF
  dw    0x0
  db    0x0
  db    10010010b
  db    11001111b
  db    0x0
gdt_end:
gdt_pointer:
  dw    gdt_end - gdt_start
  dd    gdt_start
CODE_SEG equ gdt_code - gdt_start
DATA_SEG equ gdt_data - gdt_start

bits 32

boot2:
  mov   ax,  DATA_SEG
  mov   ds,  ax
  mov   es,  ax
  mov   fs,  ax
  mov   gs,  ax
  mov   ss,  ax
  mov   esi, hello
  mov   ebx, 0xb8000

.loop:
  lodsb
  or    al,al
  jz    halt
  or    eax,0x0100
  mov   word [ebx], ax
  add   ebx,2
  jmp  .loop

halt:
  cli
  hlt

hello:
  db "Hello world!",0

times 510 - ($-$$) db 0
dw 0xaa55

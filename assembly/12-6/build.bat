@echo off

IF NOT EXIST build mkdir build

del /Q build\*

pushd build

yasm -f win64 -a x86 -m amd64 -g cv8 -X vc -Werror -Worphan-labels -o sse.obj ..\sse.asm 
link sse.obj /dll /defaultlib:msvcrt.lib /defaultlib:ucrt.lib /defaultlib:legacy_stdio_definitions.lib /defaultlib:shell32.lib /defaultlib:kernel32.lib /opt:ref /export:float_sum /incremental:no /debug:full /nologo
cl ..\main.cpp /Zi /link /subsystem:console /nologo /defaultlib:sse.lib /incremental:no /opt:noref /debug:full

popd

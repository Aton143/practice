#include <stdio.h>
#include <intrin.h>

extern "C" float float_sum(float *floats, int floats_count);

// C-translation of assembly... kinda
extern "C" float c_float_sum(float *floats, int floats_count) {
  __m128 sum = {};

  for (int i = 0; i < (floats_count / 4); i++) {
    __m128 mm_float = _mm_load_ss(&floats[4 * i]);
    sum = _mm_add_ps(sum, mm_float);
  }

  float acc = 0.0f;
  for (int j = (floats_count & (~0x3)); j < floats_count; ++j)  {
    acc += floats[j];
  }

  sum = _mm_hadd_ps(sum, sum);
  sum = _mm_hadd_ps(sum, sum);

  acc = _mm_cvtss_f32(sum) + acc;
  return acc;
}

int main(void) {
  __declspec(align(16)) float floats[] = {1.0f, 2.0f, 3.0f, 4.0f};//, 5.0f};//, 6.0f, 7.0f, 8.0f, 9.0f};
  int floats_count = (sizeof(floats) / sizeof(floats[0]));

  printf("%.25f\n", c_float_sum(floats, floats_count));
  return 0;
}

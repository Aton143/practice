bits 64
default rel

segment .text
global float_sum

float_sum:
  push   rbp
  mov    rbp,   rsp
  sub    rsp,   0x20

  pxor   xmm0,  xmm0 ; zero out xmm0
  pxor   xmm2,  xmm2

  cmp    rdx,   0x3
  jle   .rest

.loop:
  movdqa xmm1, [rcx] ; needs to be 16-byte-aligned
  add    rcx,   0x10
  
  addps  xmm0,  xmm1

  sub    rdx,   0x4
  cmp    rdx,   0x3
  jg    .loop

.rest:               ; do the remaining bytes - is there a better way?
  dec    rdx
  cmp    rdx,  -1
  je    .leave

  movss  xmm2, [rcx]
  addss  xmm0,  xmm2

  add    rcx,   0x4
  jmp   .rest

.leave:
  haddps xmm0,  xmm0 ; accumulate values on xmm0 and return
  haddps xmm0,  xmm0

  leave
  ret

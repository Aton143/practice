#include <stdio.h>
#include <stdint.h>

extern "C" int32_t boyer_moore(int *nums, int nums_count);
static int32_t c_boyer_moore(int *nums, int nums_count) {
  int candidate;
  int count = 0;

  for (int i = 0; i < nums_count; ++i) {
    if (count == 0) {
      candidate = nums[i];
    }

    count += (nums[i] == candidate) ? 1 : -1;
  }

  return candidate;
}

int main(void)
{
  int nums[] = {1, 4, 3, 1, 1};
  int nums_count = sizeof(nums) / sizeof(nums[0]);

  int32_t result = boyer_moore(nums, nums_count);
  int32_t c_result = c_boyer_moore(nums, nums_count);
  printf("%d, %d\n", result, result + c_result);
  return 0;
}

bits 64
default rel

segment .text
global boyer_moore

; the following implementation of Boyer-Moore is:
; very, very, very bad!!!!
boyer_moore:
  push rbp
  mov  rbp, rsp
  sub  rsp, 0x20

  xor  eax, eax ; candidate
  xor  rbx, rbx ; count
  xor  rsi, rsi ; index

.loop:
  mov   edi, [rcx + 4*rbx] ; rdi = nums[i]
  cmp   rbx, 0             ; if count == 0
  cmovz eax, edi           ; then mov

  cmp   rax, rdi           ; if candidate == nums[i]
  jne   .neg_one
  mov   r12, 1

.back:
  add   rbx, r12

  inc   rsi                ; increment index
  cmp   rsi, rdx           ; loop
  jne   .loop

  leave
  ret

.neg_one:
  mov   r12, -1
  jmp   .back

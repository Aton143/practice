@echo off

IF NOT EXIST build mkdir build

del /Q build\*

pushd build

yasm -a x86 -f win64 -m amd64 -g cv8 -X vc -Werror -Worphan-labels -o boyermoore.obj ..\boyermoore.asm 
link boyermoore.obj /dll /out:boyermoore.dll /opt:ref /export:boyer_moore /nologo /defaultlib:legacy_stdio_definitions.lib /defaultlib:ucrt.lib /defaultlib:msvcrt.lib /defaultlib:shell32.lib /incremental:no /debug /nologo
cl ..\main.cpp /Ox /link /subsystem:console /opt:noref /defaultlib:ucrt.lib /defaultlib:shell32.lib /defaultlib:kernel32.lib /defaultlib:boyermoore.lib /entry:main /nologo /debug 

popd

bits 64
default rel

segment .data

segment .text
global mov_10

extern _CRT_INIT
extern ExitProcess

mov_10:
  push rbp
  mov  rbp, rsp
  sub  rsp, 32

  xor  rax, rax
  mov  rax, 40

  leave
  ret

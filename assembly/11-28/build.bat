@echo off

IF NOT EXIST build mkdir build

del /Q build\*

pushd build

yasm -f win64 -g cv8 -Werror -Worphan-labels -X vc -o name.obj ../name.asm
link name.obj /dll /defaultlib:legacy_stdio_definitions.lib /defaultlib:kernel32.lib /defaultlib:msvcrt.lib /defaultlib:shell32.lib /opt:ref /export:mov_10 /nologo /incremental:no /debug /out:name.dll
cl ..\main.c /link /subsystem:console /entry:main /nologo /incremental:no /debug /machine:x64 /defaultlib:legacy_stdio_definitions.lib /defaultlib:kernel32.lib /defaultlib:msvcrt.lib /defaultlib:shell32.lib /defaultlib:ucrt.lib /opt:ref name.lib

popd

@echo off

IF NOT EXIST build mkdir build

pushd build

yasm -f win64 -m amd64 -g cv8 -X vc -Werror -o reverse.obj ..\reverse.asm
link reverse.obj /dll /defaultlib:msvcrt.lib /defaultlib:shell32 /debug:full /nologo /opt:ref /export:reverse_int32 /out:reverse.dll
REM cl ..\main.cpp /Zi /link /subsystem:console /DEBUG:FULL /nologo /incremental:no /opt:noref /defaultlib:reverse.lib

popd

bits 64
default rel

segment .data
  float_zero     dd 0.0
  float_constant dd 1.2

segment .text
global func

func:
  push  rbp
  mov   rbp, rsp

  movss xmm0, dword [float_zero]
  addss xmm0, dword [float_constant]

  leave
  ret

@echo off

IF NOT EXIST build mkdir build

del /Q build\*

pushd build

yasm -f win64 -m amd64 -g cv8 -X vc -Werror -Worphan-labels -o practice.obj ..\practice.asm
link practice.obj /dll /defaultlib:ucrt.lib  /defaultlib:msvcrt.lib /defaultlib:legacy_stdio_definitions.lib /defaultlib:shell32.lib /defaultlib:kernel32.lib /incremental:no /debug /opt:ref /export:func /nologo
cl ..\main.cpp /nologo /link /subsystem:console /debug /defaultlib:ucrt.lib  /defaultlib:shell32.lib /defaultlib:kernel32.lib /defaultlib:practice.lib /incremental:no /opt:noref /entry:main

popd

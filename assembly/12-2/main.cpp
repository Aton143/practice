#include <stdio.h>

extern "C" float func(void);

extern "C" int main(void) {
  printf("%f\n", func());
  return(0);
}

bits 64     ; 64-bit mode
default rel ; use rip-relative addressing

segment .data
  msg db "Hello, World!", 0xD, 0xA, 0

segment .text
global main        ; export main
extern ExitProcess ; import ExitProcess
extern _CRT_INIT   ; _CRT_INIT initializes all static variables and terminators

extern printf      ; import printf

main:
  push rbp         ; push rbp to stack
  mov  rbp, rsp    ; save stack pointer to rbp
  sub  rsp, 32     ; shadow space

  call _CRT_INIT

  lea  rcx, [msg]  ; get address of msg and pass it to rcx
  call printf

  xor  rax, rax    ; zero out return register
  call ExitProcess

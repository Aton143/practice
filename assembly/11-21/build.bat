@echo off

IF NOT EXIST build mkdir build
pushd build

yasm -f win64 -g cv8 -m amd64 -o hello_world.obj -Werror -Worphan-labels -X vc ../hello_world.asm
link hello_world.obj legacy_stdio_definitions.lib kernel32.lib msvcrt.lib shell32.lib /subsystem:console /out:hello_world.exe -nologo -incremental:no -opt:noref -debug

popd
